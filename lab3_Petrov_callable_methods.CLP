(defglobal ?*random_not_ordered_fact* = (nth$ (random 1 (- (length$ (get-fact-list)) 10)) (get-fact-list)))
(defglobal ?*random_ordered_fact* =  (nth$ (random (- (length$ (get-fact-list)) 10) (length$ (get-fact-list))) (get-fact-list)))
(fact-relation ?*random_not_ordered_fact*)
(fact-slot-names ?*random_not_ordered_fact*)
(fact-slot-value ?*random_not_ordered_fact* groupname)

(defglobal ?*duplicated_fact* = (duplicate ?*random_not_ordered_fact* (groupname "S")))
(fact-relation (nth$ 22 (get-fact-list)))